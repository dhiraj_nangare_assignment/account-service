package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.controller.request.CustomerRequest;
import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.CustomerTransactionsResponse;
import com.assignment.service.AccountService;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@CrossOrigin
@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	/**
	 * Endpoint to add new Customer.
	 * 
	 * @param customerReq
	 * @return
	 */
	@PostMapping(value = "/customer/add")
	public ResponseEntity<ResultMessage> addCustomer(@RequestBody CustomerRequest customerReq) {
		ResultMessage responseMessage = accountService.addCustomer(customerReq);
		return new ResponseEntity<>(responseMessage, HttpStatus.OK);
	}

	/**
	 * Endpoint to get all Customers and related Transactions.
	 * 
	 * @return
	 */
	@GetMapping(value = "/customers")
	public ResponseEntity<List<CustomerTransactionsResponse>> getAllCustomers() {
		List<CustomerTransactionsResponse> custList = accountService.getAllCustomers();
		return new ResponseEntity<>(custList, HttpStatus.OK);

	}

	/**
	 * Endpoint to create new Account and Transaction.
	 * 
	 * @param customerId
	 * @param initialCreditAmount
	 * @return
	 */
	@PostMapping(value = "/customer/initialcredit/{customerId}/{initialCreditAmount}")
	public ResponseEntity<ResultMessage> addInitialCredit(@PathVariable Long customerId,
			@PathVariable Long initialCreditAmount) {
		ResultMessage responseMessage = accountService.addInitialCredit(customerId, initialCreditAmount);
		return new ResponseEntity<>(responseMessage, HttpStatus.OK);
	}

}
