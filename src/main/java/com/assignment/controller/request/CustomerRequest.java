package com.assignment.controller.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class CustomerRequest {

	@NotNull(message = "{error.customer.firstname.notnull}")
    @NotEmpty(message = "{error.customer.firstname.notempty}")
    @Size(max =10, message = "{error.customer.firstname.size}")
	private String firstName;
	
	@NotNull(message = "{error.transactionId.notnull}")
    @NotEmpty(message = "{error.transactionId.notempty}")
    @Size(max = 10, message = "{error.customer.lastname.size}")
	private String lastName;
	
	private String emailId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "CustomerRequest [firstName=" + firstName + ", lastName=" + lastName + ", emailId=" + emailId + "]";
	}

	
	
	
	
}
