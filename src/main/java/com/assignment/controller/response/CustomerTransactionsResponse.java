package com.assignment.controller.response;

import java.util.List;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class CustomerTransactionsResponse {

	private Long custId;

	private String firstName;

	private String lastName;
	
	private String emailId;
	
	private Long accountId;
	
	private String accountType;

	private Long totalBalance;
	
	List<TransactionResponse> transactionList;

	public Long getCustId() {
		return custId;
	}

	public void setCustId(Long custId) {
		this.custId = custId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	
	
	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Long getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(Long totalBalance) {
		this.totalBalance = totalBalance;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public List<TransactionResponse> getTransactionList() {
		return transactionList;
	}

	public void setTransactionList(List<TransactionResponse> transactionList) {
		this.transactionList = transactionList;
	}

	@Override
	public String toString() {
		return "CustomerTransactionsResponse [custId=" + custId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", emailId=" + emailId + ", accountId=" + accountId + ", accountType=" + accountType
				+ ", totalBalance=" + totalBalance + ", transactionList=" + transactionList + "]";
	}

	
	
	

}
