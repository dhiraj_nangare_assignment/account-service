package com.assignment.controller.response;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class TransactionResponse {

	private Long transId;

	private String transactionType;

	private Long amount;

	private Long accountId;

	public TransactionResponse() {
	}

	public TransactionResponse(Long transId, String transactionType, Long amount, Long accountId) {
		super();
		this.transId = transId;
		this.transactionType = transactionType;
		this.amount = amount;
		this.accountId = accountId;
	}

	public Long getTransId() {
		return transId;
	}

	public void setTransId(Long transId) {
		this.transId = transId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public String toString() {
		return "TransactionResponse [transId=" + transId + ", transactionType=" + transactionType + ", amount=" + amount
				+ ", accountId=" + accountId + "]";
	}

}
