package com.assignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class for account table.
 * 
 * @author Dhiraj.Nangare
 *
 */
@Entity
@Table(name = "account")
public class Account {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long accntId;

	@Column(name = "account_type")
	private String accountType;

	@Column(name = "total_balance")
	private Long totalBalance;

	@ManyToOne
	@JoinColumn(name = "customer_id", referencedColumnName = "cust_id")
	private Customer customer;

	public Long getAccntId() {
		return accntId;
	}

	public void setAccntId(Long accntId) {
		this.accntId = accntId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public Long getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(Long totalBalance) {
		this.totalBalance = totalBalance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Account [accntId=" + accntId + ", accountType=" + accountType + ", totalBalance=" + totalBalance + ", customer="
				+ customer + "]";
	}

}
