package com.assignment.exception;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.assignment.controller.response.ErrorDetails;
import com.assignment.controller.response.ErrorResponse;
import com.assignment.controller.response.ValidationErrorResponse;
import com.assignment.model.ErrorCode;
import com.assignment.util.Constants;


/**
 * Common custom exception handler class .This class is responsible for handling any error thrown in
 * controller-service-dao flow
 * provided no try-catch block is added explicitly in between
 * This class is also responsible for handling and sending back proper message for javax validations applied on request
 * classes
 * 
 * @author Dhiraj.Nangare
 */
@ControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CommonExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleGenericException(Exception exception) {
        logger.error("Exception Caught: ", exception);

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setResultCode(Constants.INTERNAL_SERVER_ERROR_RESPONSE_CODE);
        errorResponse.setResultStatus(Constants.HTTP_STATUS_FAILURE);
        errorResponse.setMessage(Constants.INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE);

        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * this method handles MethodArgumentNotValidException exception which thrown by spring framework when javax
     * validation fails
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.setResultStatus(Constants.HTTP_STATUS_FAILURE);
        errorResponse.setMessage("Validation Failed");

        Map<String, List<ErrorDetails>> errors = new HashMap<>();

        createBadRequestExceptionResponse(ex, errors);

        errorResponse.setErrors(errors);
        ResponseEntity<Object> responseEntity = new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
        logger.debug("errorResponse is {} ", errorResponse);
        return responseEntity;
    }

    @ExceptionHandler(value = BaseException.class)
    public ResponseEntity<ErrorResponse> baseExceptionHandler(BaseException baseException) {
        logger.error("BaseException Caught: ", baseException);

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setResultCode(baseException.getErrorCode());
        errorResponse.setResultStatus(baseException.getStatus());
        errorResponse.setMessage(baseException.getMessage());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    private void createBadRequestExceptionResponse(MethodArgumentNotValidException exception,
            Map<String, List<ErrorDetails>> errorResponse) {
        for (ObjectError error : exception.getBindingResult().getAllErrors()) {
            List<ErrorDetails> errorDetailList = new ArrayList<>();
            setFieldLevelErrors(errorResponse, error, errorDetailList);

            logger.error(error.getDefaultMessage());
        }
    }

    private void setFieldLevelErrors(Map<String, List<ErrorDetails>> errorResponse, ObjectError error, List<ErrorDetails> errorDetailList) {
        String fieldName = ((FieldError) error).getField();
        ErrorDetails errorDetail = setErrorDetails(error);

        if (errorResponse.containsKey(fieldName)) {
            errorDetailList = errorResponse.get(fieldName);
            errorDetailList.add(errorDetail);
        } else {
            errorDetailList.add(errorDetail);
        }
        errorResponse.put(fieldName, errorDetailList);
    }

    private ErrorDetails setErrorDetails(ObjectError error) {
        ErrorDetails errorDetail = new ErrorDetails();
        ErrorCode errorCode = ErrorCode.getErrorCode(error.getDefaultMessage());
        errorDetail.setCode(errorCode.getCode());
        errorDetail.setMessage(errorCode.getMessage());
        return errorDetail;
    }

}
