package com.assignment.model;



/**
 * Message and Error code used for custom exception and to send the proper error message response .
 * 
 * @author Dhiraj.Nangare
 *
 */
public enum ErrorCode {


    /* Add Customer Enums */
    CC001("CC001", "Customer's first name should not be null"),
    CC002("CC002", "Customer's first name should not be empty"),
    CC003("CC003", "Customer's first name length should not be more than 10"),
    CC004("CC004", "Customer's last name should not be null"),
    CC005("CC005", "Customer's last name should not be empty"),
    CC006("CC006", "Customer's last name length should not be more than 10");
    

  

    private final String code;
    private final String message;

    private ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ErrorCode getErrorCode(String code) {
        ErrorCode[] errorCodes = ErrorCode.values();

        for (ErrorCode errorCode : errorCodes) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }

        return null;
    }

}
