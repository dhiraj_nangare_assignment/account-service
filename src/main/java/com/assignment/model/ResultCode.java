package com.assignment.model;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public enum ResultCode {

	SUCCESS("0000", "success", "Success"), 
	SUCCESS_ADD_CUSTOMER("0005", "success", "Customer added Successfully."), 
	TRANSACTION_FAILED("0001", "Failure","Error while adding transaction."),
	DUPLICATE_CUSTOMER("0002", "Failure", "Customer already in use"),
	INVALID_CUSTOMER_ID("0003", "Failure", "Invalid customer Id."),
	ADD_CUSTOMER_NOTIFICATION("0004", "Failure", "Failed to Add Customer"),;

	private String code;
	private String status;
	private String message;

	/**
	 * @param code
	 *            of type String
	 * @param status
	 *            of type String
	 * @param message
	 *            of type String
	 */
	private ResultCode(String code, String status, String message) {
		this.code = code;
		this.status = status;
		this.message = message;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	public static ResultCode getResponseStatusByCode(final String value) {
		ResultCode responseStatusToReturn = null;
		for (ResultCode resultCode : ResultCode.values()) {
			if (value != null && resultCode.getCode().equals(value)) {
				responseStatusToReturn = resultCode;
				break;
			}
		}
		return responseStatusToReturn;
	}
}
