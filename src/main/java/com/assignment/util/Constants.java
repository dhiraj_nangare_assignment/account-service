package com.assignment.util;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
public class Constants {

	
	// Constants to be defined here
    public static final String CUSTOMER_ID = "custId";
    public static final String CURRENT_ACCOUNT = "CURRENT";
    public static final String TOTAL_BALANCE = "totalBalance";
    public static final String ACCOUNT_ID = "accntId";
    public static final String HTTP_STATUS_FAILURE = "failure";
    public static final String INTERNAL_SERVER_ERROR_RESPONSE_CODE = "500";
    public static final String INTERNAL_SERVER_ERROR_RESPONSE_MESSAGE = "Internal Server Error";
}
