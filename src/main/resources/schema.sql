create table customer
(
   cust_id integer not null AUTO_INCREMENT,
   first_name varchar(255) not null,
   last_name varchar(255) not null,
   email_id varchar(50) not null,
   primary key(cust_id)
);

create table account
(
   id integer not null AUTO_INCREMENT,
   account_type varchar(255) not null,
   total_balance integer ,
   customer_id integer ,
   primary key(id),
   CONSTRAINT CUSTOMER FOREIGN KEY (customer_id) REFERENCES customer (cust_id) ON UPDATE NO ACTION ON DELETE NO ACTION
);