package com.assignment.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.assignment.controller.request.CustomerRequest;
import com.assignment.controller.request.ResultMessage;
import com.assignment.controller.response.CustomerTransactionsResponse;
import com.assignment.model.ResultCode;
import com.assignment.service.AccountService;

public class AccountControllerTest {
	
	@InjectMocks
	AccountController accountController = new AccountController();

	@Mock
	AccountService accountService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void getAllCustomersTest() {
		
		CustomerTransactionsResponse customerTransactionsResponse = new CustomerTransactionsResponse();
		List<CustomerTransactionsResponse> customerList = new ArrayList<>();
		customerList.add(customerTransactionsResponse);
		Mockito.when(accountService.getAllCustomers())
				.thenReturn(customerList);
		ResponseEntity<List<CustomerTransactionsResponse>> customersResp =  accountController.getAllCustomers();
		HttpStatus status = customersResp.getStatusCode();
		Assert.assertEquals(HttpStatus.OK, status);
	}
	
	
	@Test
	public void initialCreditTest() {
		
		ResultMessage resultMessage = new ResultMessage();
		resultMessage.setResultCode(ResultCode.SUCCESS.getCode());
		resultMessage.setMessage(ResultCode.SUCCESS.getMessage());
		resultMessage.setResultStatus(ResultCode.SUCCESS.getStatus());
		
		Mockito.when(accountService.addInitialCredit(1l, 100l))
				.thenReturn(resultMessage);
		ResponseEntity<ResultMessage> customersResp =  accountController.initialCredit(1l, 100l);
		HttpStatus status = customersResp.getStatusCode();
		Assert.assertEquals(HttpStatus.OK, status);
	}
	
	
	@Test
	public void addCustomerTest() {
		
		ResultMessage resultMessage = new ResultMessage();
		resultMessage.setResultCode(ResultCode.SUCCESS_ADD_CUSTOMER.getCode());
		resultMessage.setMessage(ResultCode.SUCCESS_ADD_CUSTOMER.getMessage());
		resultMessage.setResultStatus(ResultCode.SUCCESS_ADD_CUSTOMER.getStatus());
		
		CustomerRequest customerReq = new CustomerRequest();
		customerReq.setEmailId("test@gmail.com");
		customerReq.setFirstName("Test first name");
		customerReq.setLastName("Test last name");
		Mockito.when(accountService.addCustomer(customerReq))
				.thenReturn(resultMessage);
		ResponseEntity<ResultMessage> customersResp =  accountController.addCustomer(customerReq);
		HttpStatus status = customersResp.getStatusCode();
		Assert.assertEquals(HttpStatus.OK, status);
	}
	
}
