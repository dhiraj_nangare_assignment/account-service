package com.assignment.dao;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.dao.impl.AccountDaoImpl;
import com.assignment.entity.Account;
import com.assignment.entity.Customer;

/**
 * 
 * @author Dhiraj.Nangare
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountDaoImplTest {

	@Mock
	private EntityManager entityManagerMock;

	@InjectMocks
	private AccountDaoImpl accountDaoImpl;

	/*@Value("${query.get.customer.all}")
	private String getAllAccounts;

	@Value("${query.get.account.byCustomer}")
	private String getAccntByCustMockQuery;

	@Value("${query.update.account.totalBalance}")
	private String updateAccntBalanceMockQuery;*/

	@Test
	public void getAllAccountsTest() {

		TypedQuery mockedQuery = Mockito.mock(TypedQuery.class);
		Mockito.when(entityManagerMock.createQuery("SELECT c FROM Customer c", Account.class)).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.getResultList()).thenReturn(this.buildAccountLst());

		List<Account> accntList = accountDaoImpl.getAllAccounts();

		assertNotNull(accntList);
		Assert.assertEquals(accntList.get(0), this.buildAccountLst().get(0));
	}

	@Test
	public void getAccountByCustomerIdTest() {

		Customer customer = new Customer();
		customer.setCustId(1l);
		Query mockedQuery = Mockito.mock(Query.class);
		Mockito.when(entityManagerMock.createQuery("SELECT a FROM Account a WHERE a.customer.custId=:custId")).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.setParameter("custId", "1")).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.getResultList()).thenReturn(this.buildAccountLst());

		Account accnt = accountDaoImpl.getAccountByCustomerId(customer);

		assertNotNull(accnt);
		Assert.assertEquals(accnt, this.buildAccountLst().get(0));
	}

	@Test
	public void updateTotalBalanceTest() {

		Account existingAccount = new Account();
		existingAccount.setAccntId(1l);
		Query mockedQuery = Mockito.mock(Query.class);
		Mockito.when(entityManagerMock.createQuery("UPDATE Account accnt  SET totalBalance=:totalBalance WHERE accntId=:accntId")).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.setParameter("totalBalance", 100l)).thenReturn(mockedQuery);
		Mockito.when(mockedQuery.setParameter("accntId", 1l)).thenReturn(mockedQuery);

		Mockito.doNothing().when(mockedQuery).executeUpdate();
		accountDaoImpl.updateTotalBalance(existingAccount);
	}

	private List<Account> buildAccountLst() {

		List<Account> accntList = new ArrayList<>();
		Account account = new Account();
		account.setAccntId(1l);
		account.setAccountType("CURRENT");
		account.setTotalBalance(2000L);
		// TODO Auto-generated method stub
		accntList.add(account);
		return accntList;
	}
}
