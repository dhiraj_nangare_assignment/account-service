package com.assignment.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.assignment.controller.request.CustomerRequest;
import com.assignment.controller.request.ResultMessage;
import com.assignment.dao.AccountDao;
import com.assignment.dao.CustomerDao;
import com.assignment.entity.Customer;
import com.assignment.model.ResultCode;
import com.assignment.service.impl.AccountServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountServiceImplTest {

	@InjectMocks
	private AccountServiceImpl accountServiceImpl;

	@Mock
	private AccountDao accountDao;
	
	@Mock
	private CustomerDao customerDao;
	
	
	@Test
	public void addCustomerTest() {
		ResultMessage resultMessage = new ResultMessage();
		resultMessage.setResultCode(ResultCode.SUCCESS_ADD_CUSTOMER.getCode());
		resultMessage.setMessage(ResultCode.SUCCESS_ADD_CUSTOMER.getMessage());
		resultMessage.setResultStatus(ResultCode.SUCCESS_ADD_CUSTOMER.getStatus());
		CustomerRequest customerReq = new CustomerRequest();
		customerReq.setEmailId("test@gmail.com");
		customerReq.setFirstName("Test first name");
		customerReq.setLastName("Test last name");
		Customer customer = new Customer();
		customer.setEmailId("test@gmail.com");
		customer.setFirstName("Test first name");
		customer.setLastName("Test last name");
		
		Mockito.doNothing().when(customerDao).addCustomer(customer);
		Mockito.when(accountServiceImpl.addCustomer(customerReq)).thenReturn(resultMessage);
		ResultMessage resultMessage2 = accountServiceImpl.addCustomer(customerReq);
		assertNotNull("Actual Result should not be null", resultMessage2);
		Mockito.verify(customerDao, Mockito.times(0)).addCustomer(Mockito.any());;
	}
	
}
